#ifndef CONSTRUCT_GRID_CARTESIAN
#define CONSTRUCT_GRID_CARTESIAN

// Returns Cartesian grid
// meshgrid(((1/OverGridFactor:1/OverGridFactor:N)- N/2 -1/OverGridFactor).
// *delta + x_offset, 
// ((1/OverGridFactor:1/OverGridFactor:N)- N/2 -1/OverGridFactor).
// *delta + y_offset);
// input parameters:
// int N - N*overGridFactor - number variables in grid 
// int delta - coefficient, see formula
// int xOffset - x offset
// int yOffset - y offset 
// int overGridFactor � over sampling factor (default: 2)
// double* &grid1 � output grid
// double* &grid2 � output grid
void ConstructGridCartesian(int N, int delta, int x_offset, int y_offset, int overGridFactor, double* &grid1, double* &grid2);

#endif