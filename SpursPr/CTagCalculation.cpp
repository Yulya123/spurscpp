#include "CTagCalculation.h"
#include "../SuiteSparse/UMFPACK/Include/umfpack.h"
#include "../SuiteSparse/CHOLMOD/Include/cholmod.h"
#include <stdio.h>

void CTagCalculation(double* bReal, double* bImg, int tagSize, sparse_zi_struct* psi,
	double* psiZ, UmfpackLUParam* luParam, int mPhi, std::vector<float> &cXRes, std::vector<float> &cZRes)
{
	int status = 0;
	double* Rb = (double*)malloc(tagSize * sizeof(double));
	double* Rbz = (double*)malloc(tagSize * sizeof(double));
	double* cTagX = (double*)malloc(tagSize * sizeof(double));
	double* cTagZ = (double*)malloc(tagSize * sizeof(double));
	double* cTagXRes = (double*)calloc(tagSize, sizeof(double));
	double* cTagZRes = (double*)calloc(tagSize, sizeof(double));


	// (R \ b_tag)
	status = umfpack_zi_scale(Rb, Rbz, (const double*)bReal, (const double*)bImg, luParam->numeric);
	if (status < 0) printf("umfpack_zi_scale failed");
	// solve Ly = P*(Rb) 
	// (L \ (P * (R \ b_tag)))
	status = umfpack_zi_solve(UMFPACK_Pt_L, (const int*)psi->p, (const int*)psi->i, (const double*)psi->x, (const double*)psiZ, cTagX, cTagZ, Rb, Rbz,
		luParam->numeric, luParam->control, luParam->info);
	if (status < 0) printf("umfpack_zi_solve failed");
	free(Rb);
	free(Rbz);
	// solve UQ'x=y 
	status = umfpack_zi_solve(UMFPACK_U_Qt, (const int*)psi->p, (const int*)psi->i, (const double*)psi->x, (const double*)psiZ, cTagXRes, cTagZRes, cTagX, cTagZ,
		luParam->numeric, luParam->control, luParam->info);
	
	free(cTagX);
	free(cTagZ);

	int cSize = tagSize - mPhi;
	for (int i = 0; i < cSize; i++)
	{
		cXRes[i] = (float)cTagXRes[i + mPhi];
		cZRes[i] = (float)cTagZRes[i + mPhi];
	}
	free(cTagXRes);
	free(cTagZRes);
}
