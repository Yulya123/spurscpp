#pragma once
#include "..\SuiteSparse\CHOLMOD\Include\cholmod.h"
#include "DefFile.h"

// Load Psi matrix from file if it was save in the previous iteration or 
// calculate new Psi matrix
// cholmod_common* c - the Common object contains workspace that is
// used between calls to * CHOLMOD routines. 
// int fileNum - file id number
// double * ReconstructionGridCoordinates1 � reconstruction grid coordinates 
// double * ReconstructionGridCoordinates2 � reconstruction grid coordinates
// int lengthReconstrCoord - reconstruction coordinates length 
// double * SamplingGridCoordinates1 - sampling grid coordinates 
// double * SamplingGridCoordinates2 - sampling grid coordinates
// int lengthSampCoord � sampling coordinates length 
// double rho - regularization parameter (default: 0.001)
sparse_zi_struct* ReturnPsi(cholmod_common* c,  int fileNum,
	double* ReconstructionGridCoordinates1, double* ReconstructionGridCoordinates2, int lengthReconstrCoord,
	double * SamplingGridCoordinates1, double * SamplingGridCoordinates2, int lengthSampCoord, double rho);