#pragma once

// Calculate the LSI (Linear Shift Invariant) filter for use in the online stage
// input parameters:
// int kernelFunctionDegree � degree of the kernel function
// int overGridFactor- over sampling factor (default: 2)
// int sqrtN - points per row / column of the cartesian grid
double* ReturnFilter(int kernelFunctionDegree, int overGridFactor, int sqrtN);