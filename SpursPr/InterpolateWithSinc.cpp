#include <iostream>
#include <vector>
#include "mkl.h"

#include <stdio.h>
#include <stdlib.h>
#include "../SuiteSparse/CHOLMOD/Include/cholmod.h"
#include "../SuiteSparse/UMFPACK/Include/umfpack.h"
#include <ctime>
#define _USE_MATH_DEFINES // pi
#include <math.h>         // sin 
#define min(x,y) (((x) < (y)) ? (x) : (y))

void SincMeshgrid(size_t nSizeUnif, size_t stepUnif, const double* uniformGridPoints,
	size_t nSizeNonUnif, const double* nonUniformGridPoints, double* matOut)
{
	double* pkUniform = new double[nSizeUnif*nSizeNonUnif];
	double* pkNonUniform = new double[nSizeNonUnif*nSizeUnif];

	int indexTemp = 0;

	for (int row = 0; indexTemp < nSizeUnif*nSizeNonUnif; row = row + stepUnif)
	{
		for (int i = 0; i < nSizeNonUnif; i++)
		{
			pkUniform[indexTemp] = uniformGridPoints[row];
			indexTemp++;
		}
	}
	indexTemp = 0;
	for (int i = 0; i < nSizeUnif; i++)
	{
		for (int row = 0; row < nSizeNonUnif; row++)
		{
			pkNonUniform[indexTemp] = nonUniformGridPoints[row];
			indexTemp++;
		}
	}

	int matrSize = nSizeUnif*nSizeNonUnif;

	for (int i = 0; i < matrSize; i++)
	{
		double diff = pkUniform[i] - pkNonUniform[i];
		if (std::abs(diff) > 1.0020841800044864E-292)
		{
			matOut[i] = (sin(M_PI*diff) / (M_PI*diff)) / (double)nSizeUnif;
		}
		else
		{
			matOut[i] = 1/ (double)nSizeUnif;
		}
	}
	delete[] pkUniform;
	delete[] pkNonUniform;
}


void InterpolateWithSinc(int numVar, const double* uniformGridPoints1, const double* uniformGridPoints2,
	const double* uniformSampleValuesRe, const double* uniformSampleValuesIm,
	int numNonUnifGridPoint, const double* nonUniformGridPoints1, const double* nonUniformGridPoints2, 
	double* nonUniformSampleValuesOutRe, double* nonUniformSampleValuesOutIm)
{
	std::vector<double> rowMat(numVar*numNonUnifGridPoint, 0);

	double *colMat, *BRe, *BIm, *CRe, *CIm;
	int  m = numVar;
	int ka = numNonUnifGridPoint;
	int kb = numVar;
	int n = numVar;
	double alpha = 1.0; double beta = 0.0;
	colMat = (double *)mkl_calloc(m*ka, sizeof(double), 64); //A
	BRe = (double *)mkl_malloc(kb*n * sizeof(double), 64);
	BIm = (double *)mkl_malloc(kb*n * sizeof(double), 64);
	CRe = (double *)mkl_calloc(ka*kb, sizeof(double), 64);
	CIm = (double *)mkl_calloc(ka*kb, sizeof(double), 64);
	if (colMat == NULL || BRe == NULL || BRe == NULL || CRe == NULL || CRe == NULL) {
		printf("\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
		mkl_free(colMat);
		mkl_free(BRe);
		mkl_free(BIm);
		mkl_free(CRe);
		mkl_free(CIm);
		return;
	}

	SincMeshgrid(numVar, 1, uniformGridPoints2, numNonUnifGridPoint, nonUniformGridPoints2, colMat);
	// colMat - col_mat = sinc(pku_c-pknu_c)/ I. / N;;
	
	SincMeshgrid(numVar, numVar, uniformGridPoints1, numNonUnifGridPoint, nonUniformGridPoints1, rowMat.data());
	
	for (int i = 0; i < (kb*n); i++) {
		BRe[i] = uniformSampleValuesRe[i];
	}

	for (int i = 0; i < (kb*n); i++) {
		BIm[i] = uniformSampleValuesIm[i];
	}

	cblas_dgemm(CblasRowMajor, CblasTrans, CblasTrans,
		ka, n, m, alpha, colMat, ka, BRe, n, beta, CRe, n);
	cblas_dgemm(CblasRowMajor, CblasTrans, CblasTrans,
		ka, n, m, alpha, colMat, ka, BIm, n, beta, CIm, n);

	mkl_free(colMat);
	mkl_free(BRe);
	mkl_free(BIm);


	for (int i = 0; i < numNonUnifGridPoint; i++)
	{
		float sumRe = 0.0;
		float sumIm = 0.0;
		for (int j = 0; j < numVar; j++)
		{
			sumRe = sumRe + rowMat[j*numNonUnifGridPoint + i] * CRe[i*numVar + j];
			sumIm = sumIm + rowMat[j*numNonUnifGridPoint + i] * CIm[i*numVar + j];
		}
		nonUniformSampleValuesOutRe[i] = sumRe;
		nonUniformSampleValuesOutIm[i] = sumIm;
	}
	mkl_free(CRe);
	mkl_free(CIm);
}