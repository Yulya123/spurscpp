# SpursCPP

	How to run SpursCpp on Windows ?
		
		******************************************************************************************************************************
		* Requirements: 
		*     - IPP
		* 	  - MKL libraries
		*	  - MATLAB 2017a (and above)
		*	  - Visual Studio 2015 x64
		******************************************************************************************************************************
		
		1. Download the folder 'Source' from git repo to your computer (see 3. Links)
	
		2. Install IPP and MKL libraries (This file is free to use after registration to the Intel site) (see 3. Links)
	
		<!-- - Important - -->
		******************************************************************************************************************************
		* Add to "Path" under "User environment" :
		* for example the default path at 64bit : 
		*	ipp : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\ipp
		*	mkl : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\mkl
		*	compiler : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\compiler
		******************************************************************************************************************************
		
		3. Links:
			a. Git repo : https://bitbucket.org/Yulya123/spurscpp.git
			b. IPP : https://software.intel.com/en-us/intel-ipp
			c. MKL libraries : https://software.intel.com/en-us/mkl
	
		4. Insert your input files to
			InputData\b.mtx (vector b, of k-space samples)
			InputData\SamplingGridCoordinates1.mtx
			InputData\SamplingGridCoordinates2.mtx 
			
		5. Change if needed settings in 
			InputData\SpursSettings.txt
				numIterations - number of algorithm iteration
				sqrtN - points per row / column of the cartesian grid
				overGridFactor - over sampling factor
				rho - regularization parameter
		
		6. To run algorithm:
		
			a. -- without display image ---
				Run	Release\SpursPr.exe
				Look on output file in OutputData
	
			b. -- with display image ---
				Run Release\RunAndDisplay.bat
				to compare image with reference, put refImage into path : RefImage\refImage.mtx (refImage is just column-oriented order matrix without comments and size)

		******************************************************************************************************************************
		* Input file are in "mtx" format column-oriented order for example
		* A = [1,2;3,4] (in MATLAB)
		* %%MatrixMarket matrix array real general
		*		2 2
		*		1
		*		3
		*		2
		*		4
		******************************************************************************************************************************
	  
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
	Development Part (tested on VS2015 x64):

		1. Install IPP and MKL libraries (This file is free to use after registration to the Intel site)- see "Important" above.
		2. Install SuiteSparse library (Tim Davis) 
		3. Include SpursPr into SuiteSparse solution
			…\SuiteSparse
			…\SpursPr
		4. Put input files to
			…\InputData\b.mtx
			…\InputData\SamplingGridCoordinates1.mtx
			…\InputData\SamplingGridCoordinates2.mtx
		5. Build and run solution, output file will be in 
			…\OutputData

		
